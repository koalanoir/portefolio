import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {
  photos_capge:string[]=[
    '../../assets/images/capgeminilogo.png',
    '../../assets/images/ah.jpg',
    '../../assets/images/ca.jpg'
  ]
  currentPhotoCapge:string='../../assets/images/capgeminilogo.png';
  constructor() { }

  ngOnInit(): void {
  }
  changeImageByTime(time: number,images: string[]):number {
    return time % images.length;
  }
    intervalId: any = setInterval(() => {
    const now = new Date();
    const currentHour = now.getSeconds();
    this.currentPhotoCapge = this.photos_capge[this.changeImageByTime(currentHour,this.photos_capge)];
  }, 4000);
}
