import { Component, OnInit } from '@angular/core';

import {TranslateService} from '@ngx-translate/core';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(private translate: TranslateService) {
    translate.addLangs(['fr', 'en']);
    if (localStorage.getItem('locale')) {
        translate.setDefaultLang(localStorage.getItem('locale')|| '{}');
        translate.use(localStorage.getItem('locale')|| '{}');
    } else {
        translate.setDefaultLang('en');
        translate.use('en');
        localStorage.setItem('locale', 'en');
    }
  }
  
  changeLanguage(language: string){
    this.translate.use(language);
    this.translate.setDefaultLang(language);
    localStorage.setItem('locale', language);
  }


  ngOnInit(): void {
  }

}
