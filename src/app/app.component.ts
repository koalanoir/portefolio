import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'portefolio Dorian / koala noir';
  constructor(private translate: TranslateService) {
    translate.addLangs(['fr', 'en']);
    if (localStorage.getItem('locale')) {
        translate.setDefaultLang(localStorage.getItem('locale')|| '{}');
        translate.use(localStorage.getItem('locale')|| '{}');
    } else {
        translate.setDefaultLang('en');
        translate.use('en');
        localStorage.setItem('locale', 'en');
    }
  }
  showLoading = true;

  ngOnInit() {
    // Utilisation de window.onload pour masquer la page de chargement
    window.onload = () => {
      this.showLoading = false;
    };
  }
  changeLanguage(language: string){
    this.translate.use(language);
    this.translate.setDefaultLang(language);
    localStorage.setItem('locale', language);
  }
}
