import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import{ TranslateModule} from '@ngx-translate/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { WorkComponent } from './work/work.component';
import { WritingComponent } from './writing/writing.component';
import { ContactComponent } from './contact/contact.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import { TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { CompanyComponent } from './company/company.component';
import { HelloworldComponent } from './helloworld/helloworld.component';
import { ProjectsComponent } from './projects/projects.component';
import { LoadingComponent } from './loading/loading.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    WorkComponent,
    WritingComponent,
    ContactComponent,
    NavbarComponent,
    FooterComponent,
    CompanyComponent,
    HelloworldComponent,
    ProjectsComponent,
    LoadingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }


export function HttpLoaderFactory(http:HttpClient){
  return new TranslateHttpLoader(http);
}